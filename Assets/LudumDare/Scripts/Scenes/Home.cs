﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Home : MonoBehaviour
{

    public Text HelpMoveText;
    public Text HelpJumpText;
    public Text HelpDuckText;
    public Text HelpUseText;

	// Use this for initialization
	void Start ()
    {
        HelpMoveText.text = string.Format("Press {0} to Move Left\nPress {1} to Move Right",
            GameManager.Instance.GameSettings.KeyBindingLeft.ToString(),
            GameManager.Instance.GameSettings.KeyBindingRight.ToString());

        HelpJumpText.text = string.Format("Press {0} to Jump",
            GameManager.Instance.GameSettings.KeyBindingJump.ToString());

        HelpDuckText.text = string.Format("Press {0} to Duck",
            GameManager.Instance.GameSettings.KeyBindingDuck.ToString());

        HelpUseText.text = string.Format("Press {0} to Activate\n!Not Working! :(",
            GameManager.Instance.GameSettings.KeyBindingUse.ToString());

        GameManager.Instance.MusicController.PlayMusic("Test");

        GameManager.Instance.GuiController.Enable = false;
	}


	
}
