﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Lose : MonoBehaviour
{
    public void Restart()
    {
        SceneManager.LoadScene("Bootstrap");
    }
}
