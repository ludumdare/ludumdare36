﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class Bootstrap : MonoBehaviour
{
    public SettingsData Settings;
    public string StartingScene;

    void Start()
    {
        //  load default settings
        GameManager.Instance.GameSettings = Settings;

        //  load custom settings
        SceneManager.LoadScene(StartingScene);

        GameManager.Instance.MusicController.SetVolume(GameManager.Instance.GameSettings.MusicVolume);
        GameManager.Instance.SoundController.SetVolume(GameManager.Instance.GameSettings.SoundVolume);
    }

}
