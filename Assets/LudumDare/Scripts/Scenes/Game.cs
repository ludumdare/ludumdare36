﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Sdn.Logging;
using CreativeSpore.SuperTilemapEditor;

public class Game : MonoBehaviour
{
    public RoomDatabase RoomDatabase;

    public void TryLoad()
    {
        D.Trace("[Game] TryLoad");
        GameManager.Instance.GameController.StopRoom();
        GameManager.Instance.GameController.OpenRoom("Room0");
        GameManager.Instance.GameController.StartRoom();
    }

    public void TryRestart()
    {
        D.Trace("[Game] TryLoad");
        GameManager.Instance.GameController.RestartRoom();
    }

    void Start ()
    {
        D.Trace("[Game] Start");
        LogManager.Instance.ShowWatch = true;
        GameManager.Instance.GameController.StartGame();
	}

}
