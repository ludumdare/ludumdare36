﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour
{
    public ObjectDoor Door1;
    public ObjectDoor Door2;
    public ObjectDoor Door3;
    public ObjectDoor Door4;

    public ObjectDoorPortal DoorPortal;

    public ObjectDoorFinal DoorFinal;

    public SpriteRenderer RuneO;
    public SpriteRenderer RuneP;
    public SpriteRenderer RuneE;
    public SpriteRenderer RuneN;

    private GameData _gameData;

    void Start()
    {
        _gameData = GameManager.Instance.GameController.Data;

        if (_gameData.Door1State == DoorStates.DOOR_STATE_ACTIVE)
        {
            Door1.Open();
        }

        if (_gameData.Door2State == DoorStates.DOOR_STATE_ACTIVE)
        {
            Door2.Open();
            RuneO.enabled = true;
        }

        if (_gameData.Door3State == DoorStates.DOOR_STATE_ACTIVE)
        {
            Door3.Open();
            RuneO.enabled = true;
            RuneP.enabled = true;
        }

        if (_gameData.Door4State == DoorStates.DOOR_STATE_ACTIVE)
        {
            Door4.Open();
            RuneO.enabled = true;
            RuneP.enabled = true;
            RuneE.enabled = true;
        }

        //  check if the final door is open

        bool _final = true;

        if (_gameData.Door1State != DoorStates.DOOR_STATE_COMPLETE)
        {
            _final = false;
        }

        if (_gameData.Door2State != DoorStates.DOOR_STATE_COMPLETE)
        {
            _final = false;
        }

        if (_gameData.Door3State != DoorStates.DOOR_STATE_COMPLETE)
        {
            _final = false;
        }

        if (_gameData.Door4State != DoorStates.DOOR_STATE_COMPLETE)
        {
            _final = false;
        }

        if (_final)
        {
            DoorFinal.Open();
            RuneO.enabled = true;
            RuneP.enabled = true;
            RuneE.enabled = true;
            RuneN.enabled = true;
        }

        if (GameManager.Instance.GameSettings.Cheats)
        {
            Door1.Close();
            Door2.Close();
            Door3.Close();
            Door4.Close();
            DoorFinal.Open();
        }
        GameManager.Instance.GuiController.Enable = true;
    }

    void OnEnable()
    {
        RuneO.enabled = false;
        RuneP.enabled = false;
        RuneE.enabled = false;
        RuneN.enabled = false;
    }


}
