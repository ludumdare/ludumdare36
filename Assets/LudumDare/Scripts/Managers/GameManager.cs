﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoSingleton<GameManager>
{
    public PlayerController PlayerController { get; set; }
    public CameraController CameraController { get; set; }
    public InputController InputController { get; set; }
    public GameController GameController { get; set; }
    public GuiController GuiController { get; set; }
    public SoundController SoundController { get; set; }
    public MusicController MusicController { get; set; }
    public PrefabController PrefabController { get; set; }
    public SettingsData GameSettings { get; set; }
}
