﻿using UnityEngine;
using System.Collections;

public class GameData
{
    public int Score { get; set; }
    public int Lives { get; set; }
    public int Difficulty { get; set; }
    public DoorStates Door1State { get; set; }
    public DoorStates Door2State { get; set; }
    public DoorStates Door3State { get; set; }
    public DoorStates Door4State { get; set; }
    public DoorStates DoorFinalState { get; set; }

    public bool FinalDoorOpen { get; set; }

    public GameData()
    {
        Score = 0;
        Lives = 3;
        Difficulty = 1;
        Door1State = DoorStates.DOOR_STATE_ACTIVE;
        Door2State = DoorStates.DOOR_STATE_INACTIVE;
        Door3State = DoorStates.DOOR_STATE_INACTIVE;
        Door4State = DoorStates.DOOR_STATE_INACTIVE;
        FinalDoorOpen = false;
    }
}
