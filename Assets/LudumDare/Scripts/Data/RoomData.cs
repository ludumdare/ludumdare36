﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
[System.Serializable]
public class RoomData : ScriptableObject
{
    [SerializeField]
    public string Scene;

    [SerializeField]
    public string RoomName;

}
