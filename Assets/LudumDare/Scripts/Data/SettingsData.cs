﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
[System.Serializable]
public class SettingsData : ScriptableObject
{
    [SerializeField]
    public KeyCode KeyBindingLeft;

    [SerializeField]
    public KeyCode KeyBindingRight;

    [SerializeField]
    public KeyCode KeyBindingJump;

    [SerializeField]
    public KeyCode KeyBindingDuck;

    [SerializeField]
    public KeyCode KeyBindingUse;

    [SerializeField]
    public string StartingRoom;

    [SerializeField]
    public int TotalLives;

    [SerializeField]
    public int DifficultLevel;

    [SerializeField]
    public int HiScore;

    [SerializeField]
    public bool Cheats;

    [SerializeField]
    public bool Arena;

    [SerializeField]
    public float MusicVolume;

    [SerializeField]
    public float SoundVolume;

}
