﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu]
[System.Serializable]
public class RoomDatabase : ScriptableObject
{
    [SerializeField]
    public List<RoomData> Rooms;
}
