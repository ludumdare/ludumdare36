﻿using UnityEngine;
using System.Collections;

public enum DoorTypes
{
    DOOR_ROOM,
    DOOR_PORTAL,
    DOOR_FINAL,
}

public enum DoorStates
{
    DOOR_STATE_INACTIVE,
    DOOR_STATE_ACTIVE,
    DOOR_STATE_COMPLETE,
}

[CreateAssetMenu]
[System.Serializable]
public class DoorData : ScriptableObject
{
    [SerializeField]
    public DoorTypes Type;

    //  name of the room to open
    [SerializeField]
    public string Room;

    //  name of the door to exit from
    [SerializeField]
    public string Door;
}
