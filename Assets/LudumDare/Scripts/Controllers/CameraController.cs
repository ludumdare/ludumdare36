﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Vector2 Offset;
    public float Speed = 1.0f;

    private PlayerController _player;

    private bool _running;

    void LateUpdate()
    {

        if (!_running)
            return;

        Vector2 pos;
        pos = _player.transform.position;
        pos.x += Offset.x;
        pos.y += Offset.y;

        Vector2 newpos = Vector2.Lerp(transform.position, pos, Speed * Time.deltaTime);
        transform.position = newpos;
        D.Watch("Camera Pos", newpos);
    }

    public void OnStart()
    {
        D.Trace("[CameraController] OnStart");
        _running = true;
    }

    public void OnStop()
    {
        D.Trace("[CameraController] OnStop");
        _running = false;
    }
	
    void Start()
    {
        D.Trace("[CameraController] Start");
        _player = GameManager.Instance.PlayerController;
    }

    void OnEnable()
    {
        D.Trace("[CameraController] OnEnable");
        GameManager.Instance.CameraController = this;
    }
}
