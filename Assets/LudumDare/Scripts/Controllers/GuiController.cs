﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GuiController : MonoBehaviour
{
    public Camera GuiCamera;
    public Text ScoreText;
    public Text LivesText;
    public bool Enable;

    private Coroutine _debug;
    private Coroutine _run;

	private IEnumerator debug()
    {
        D.WatchGroup("Gui", null);
        D.Watch("Gui", "Lives", GameManager.Instance.GameController.Data.Lives);
        yield return new WaitForSeconds(0.25f);
    }

    private IEnumerator run()
    {
        while (true)
        {
            ScoreText.text = GameManager.Instance.GameController.Data.Score.ToString();
            LivesText.text = "x " + GameManager.Instance.GameController.Data.Lives.ToString();
            GuiCamera.enabled = Enable;
            yield return new WaitForSeconds(0.25f);
        }
    }

    // Use this for initialization
	public void OnStart()
    {
        _debug = StartCoroutine(debug());
        _run = StartCoroutine(run());
	}

    public void OnStop()
    {
        StopCoroutine(_run);
        StopCoroutine(_debug);
    }
	
	// Update is called once per frame
	void OnEnable ()
    {
        GameManager.Instance.GuiController = this;
	}
}
