﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    private PlayerController _player;

    private Coroutine _run;
    private bool _keyPressed;

    public void OnStart()
    {
        D.Trace("[InputController] OnStart");
        _run = StartCoroutine(run());
    }

    public void OnStop()
    {
        D.Trace("[InputController] OnStop");
        StopCoroutine(_run);
    }

	private IEnumerator run ()
    {
        D.Trace("[InputController] run");

        yield return new WaitForSeconds(1.0f);
        while (true)
        {
            if (Input.anyKey)
            {
                _keyPressed = true;

                if (Input.GetKey(GameManager.Instance.GameSettings.KeyBindingDuck))
                {
                    _player.Duck();
                }
                else
                {
                    if (Input.GetKeyDown(GameManager.Instance.GameSettings.KeyBindingJump))
                    {
                        _player.Jump();
                    }

                    if (Input.GetKey(GameManager.Instance.GameSettings.KeyBindingLeft))
                    {
                        _player.MoveLeft();
                    }

                    if (Input.GetKey(GameManager.Instance.GameSettings.KeyBindingRight))
                    {
                        _player.MoveRight();
                    }
                }

            }
            else
            {
            _player.Stop();
            }
            yield return null;
        }
	}

    void Start()
    {
        D.Trace("[InputController] Start");
        _player = GameManager.Instance.PlayerController;
    }

    void OnEnable()
    {
        D.Trace("[InputController] OnEnable");
        GameManager.Instance.InputController = this;
    }
}
