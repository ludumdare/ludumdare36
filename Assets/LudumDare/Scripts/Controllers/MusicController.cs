﻿using UnityEngine;
using System.Collections.Generic;

public class MusicController : MonoBehaviour
{
    public AudioClip[] Clips;

    private AudioSource _audioSource;

    private Dictionary<string, AudioClip> _clips;

    private float _volume = 1.0f;

    public void PlayMusic(string name)
    {
        playMusic(_clips[name]);
    }

    public void PlayMusic(string name, float volume)
    {
        playMusic(_clips[name], 1.0f, volume);
    }

    public void SetVolume(float volume)
    {
        _volume = volume;
    }

    //	PRIVATE

    private void playMusic(AudioClip clip)
    {

        playMusic(clip, 1.0f, _volume);
    }

    private void playMusic(AudioClip clip, float pitch, float volume)
    {

        _audioSource.clip = clip;
        _audioSource.volume = volume;
        _audioSource.pitch = pitch;
        _audioSource.loop = true;
        _audioSource.Play();
        name = string.Format("Music ({0})", clip.name);
    }

    void Start()
    {
        _clips = new Dictionary<string, AudioClip>();
        foreach (AudioClip clip in Clips)
        {
            _clips.Add(clip.name, clip);
        }
    }

    void OnEnable()
    {
        D.Trace("[MusicController] OnEnable");
        GameManager.Instance.MusicController = this;
        _audioSource = gameObject.AddComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    void OnDisable()
    {
    }
}
