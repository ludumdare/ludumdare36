﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    public GameData Data { get; set; }
    
    private List<RoomTrap> _roomTraps;
    private List<RoomObject> _roomObjects;
    private List<RoomItem> _roomItems;
    private List<RoomEnemy> _roomEnemies;

    private string _lastRoom;

    public void RegisterRoomTrap(RoomTrap roomTrap)
    {
        D.Trace("[GameController] RegisterRoomTrap");
        D.Detail("- registering room trap {0}", roomTrap.name);
        _roomTraps.Add(roomTrap);
    }

    public void RegisterRoomObject(RoomObject roomObject)
    {
        D.Trace("[GameController] RegisterRoomObject");
        D.Detail("- registering room object {0}", roomObject.name);
        _roomObjects.Add(roomObject);
    }

    public void RegisterRoomItem(RoomItem roomItem)
    {
        D.Trace("[GameController] RegisterRoomItem");
        D.Detail("- registering room item {0}", roomItem.name);
        _roomItems.Add(roomItem);
    }

    public void RegisterRoomEnemy(RoomEnemy roomEnemy)
    {
        D.Trace("[GameController] RegisterRoomenemy");
        D.Detail("- registering room enemy {0}", roomEnemy.name);
        _roomEnemies.Add(roomEnemy);
    }
    public void StartGame()
    {
        D.Trace("[GameController] StartGame");

        Data = new GameData();
        string room = GameManager.Instance.GameSettings.StartingRoom;

        if (GameManager.Instance.GameSettings.Arena)
        {
            room = "Arena";
            Data.Lives = 99;
        }

        initGameStuff();
        OpenRoom(room);
        StartRoom();
    }

    public void EndGame()
    {
        D.Trace("[GameController] EndGame");
        SceneManager.LoadScene("Lose");
    }

    public void RestartRoom()
    {
        D.Trace("[GameController] RestartRoom");
        D.Detail("- player has {0} lives remaining", Data.Lives);

        if (Data.Lives <= 0)
        {
            EndGame();
        }
        else
        {
            StopRoom();
            OpenRoom(_lastRoom);
            StartRoom();
        }
    }

    public void StartRoom()
    {
        D.Trace("[GameController] StartRoom");
        StartCoroutine(startRoom());
    }

    public void StopRoom()
    {
        stopRoomStuff();
    }

    public void OpenRoom(string roomName)
    {
        D.Trace("[GameController] OpenRoom");

        if (roomName == _lastRoom)
            return;

        _roomTraps = new List<RoomTrap>();
        _roomObjects = new List<RoomObject>();
        _roomItems = new List<RoomItem>();
        _roomEnemies = new List<RoomEnemy>();

        if (!string.IsNullOrEmpty(_lastRoom))
        {
            D.Detail("- trying to remove last scene {0}", _lastRoom);
            SceneManager.UnloadScene(_lastRoom);
        }

        D.Detail("- trying to load scene {0}", roomName);

        SceneManager.LoadScene(roomName, LoadSceneMode.Additive);
        _lastRoom = roomName;
    }

    private void initGameStuff()
    {
    }

    private void initRoomStuff()
    {
        D.Trace("[GameController] initRoomStuff");
        foreach (RoomObject r in _roomObjects)
        {
            r.Init(); ;
        }
        foreach (RoomTrap r in _roomTraps)
        {
            r.Init(); ;
        }
        foreach (RoomItem r in _roomItems)
        {
            r.Init(); ;
        }
        foreach (RoomEnemy r in _roomEnemies)
        {
            r.Init(); ;
        }
    }

    private void startRoomStuff()
    {
        D.Trace("[GameController] startRoomStuff");
        foreach (RoomObject r in _roomObjects)
        {
            r.OnStart();
        }
        foreach (RoomTrap r in _roomTraps)
        {
            r.OnStart();
        }
        foreach (RoomItem r in _roomItems)
        {
            r.OnStart();
        }
        foreach (RoomEnemy r in _roomEnemies)
        {
            r.OnStart();
        }

        GameManager.Instance.CameraController.OnStart();
        GameManager.Instance.GuiController.OnStart();
        GameManager.Instance.PlayerController.OnStart();
        GameManager.Instance.InputController.OnStart();
    }

    private void stopRoomStuff()
    {
        D.Trace("[GameController] stopRoomStuff");

        GameManager.Instance.PlayerController .OnStop();
        GameManager.Instance.CameraController.OnStop();
        GameManager.Instance.InputController.OnStop();
        GameManager.Instance.GuiController.OnStop();

        foreach (RoomObject r in _roomObjects)
        {
            r.OnStop();
        }
        foreach (RoomTrap r in _roomTraps)
        {
            r.OnStop();
        }
        foreach (RoomItem r in _roomItems)
        {
            r.OnStop();
        }
        foreach (RoomEnemy r in _roomEnemies)
        {
            r.OnStop();
        }
    }

    private IEnumerator startRoom()
    {
        D.Trace("[GameController] startRoom");
        //  wait two frames for scene to load
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        initRoomStuff();
        yield return new WaitForEndOfFrame();
        startRoomStuff();
        yield return null;
    }

    void OnEnable()
    {
        D.Trace("[GameController] OnEnable");
        GameManager.Instance.GameController = this;
    }

}
