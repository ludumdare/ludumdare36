﻿using UnityEngine;
using System.Collections;
using Sdn.SpriteBoss;
using Sdn.SpriteBoss.Modules;
using DG.Tweening;

public enum PlayerDeathTypes
{
    PLAYER_DEATH_RANDOM = -1,
    PLAYER_DEATH_IMPALING = 0,
    PLAYER_DEATH_CRUSHING = 1,
    PLAYER_DEATH_CUTTING = 2,
    PLAYER_DEATH_EXPLODING = 3,
    PLAYER_DEATH_MELTING = 4,
    PLAYER_DEATH_BEHEADING = 5,
}

public enum PlayerAnimationStates
{
    PLAYER_ANIM_IDLE,
    PLAYER_ANIM_MOVING,
    PLAYER_ANIM_JUMPING,
    PLAYER_ANIM_FALL,
    PLAYER_ANIM_DUCKING,
}

public enum PlayerStates
{
    PLAYER_IDLE,
    PLAYER_DUCKING,
    PLAYER_JUMPING,
    PLAYER_FALLING,
    PLAYER_GROUNDED,
}

public enum PlayerMovementStates
{
    PLAYER_MOVE_IDLE,
    PLAYER_MOVE_LEFT,
    PLAYER_MOVE_RIGHT,
    PLAYER_MOVE_FALL,
}

public class PlayerController : MonoBehaviour
{
    public float Gravity = 5.0f;
    public float MovementSpeed;
    public float MovementDecay;
    public float JumpHeight;
    public float JumpSpeed;
    public float JumpDecay;
    public float RaycastLengthBottom = 10.5f;
    public float RaycastLengthSides = 10.5f;
    public float PlatformOffset;
    public float LiftOffset;
    public LayerMask PlatformLayer;

    private PlayerStates _state;
    private PlayerStates _lastState;
    private PlayerAnimationStates _animationState;
    private PlayerAnimationStates _lastAnimationState;
    private PlayerMovementStates _movementState;
    private PlayerMovementStates _lastMovementState;

    private bool _running;

    private Coroutine _run;
    private Coroutine _animate;
    private Coroutine _jump;
    private Coroutine _kill;

    private SpriteBoss _sprite;
    private SpriteBossModuleAnimate _animateModule;
    private SpriteRenderer _renderer;

    public void MoveLeft()
    {
        D.Trace("[PlayerController] MoveLeft");
        _animationState = PlayerAnimationStates.PLAYER_ANIM_MOVING;
        _movementState = PlayerMovementStates.PLAYER_MOVE_LEFT;
        _lastAnimationState = _animationState;
        _lastMovementState = _movementState;
        _renderer.flipX = true;
    }

    public void MoveRight()
    {
        D.Trace("[PlayerController] MoveRight");
        _animationState = PlayerAnimationStates.PLAYER_ANIM_MOVING;
        _movementState = PlayerMovementStates.PLAYER_MOVE_RIGHT;
        _lastAnimationState = _animationState;
        _lastMovementState = _movementState;
        _renderer.flipX = false;
    }

    public void Jump()
    {
        D.Trace("[PlayerController] Jump");
        //if (_state != PlayerStates.PLAYER_JUMPING && _state != PlayerStates.PLAYER_FALLING)
        if (_state == PlayerStates.PLAYER_GROUNDED)
        {
            GameManager.Instance.SoundController.PlaySound("Jump");
            _lastState = _state;
            _lastAnimationState = _animationState;
            _lastMovementState = _movementState;
            _animationState = PlayerAnimationStates.PLAYER_ANIM_JUMPING;
            _state = PlayerStates.PLAYER_JUMPING;
            _jump = StartCoroutine(jump());
        }
    }

    public void OnStart()
    {
        D.Trace("[PlayerController] OnStart");
        _animationState = PlayerAnimationStates.PLAYER_ANIM_IDLE;
        _state = PlayerStates.PLAYER_IDLE;
        _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
        _running = true;
        _run = StartCoroutine(run());
        _animate = StartCoroutine(animate());

    }

    public void OnStop()
    {
        D.Trace("[PlayerController] OnStop");

        Stop();
        _running = false;

        if (_run != null)
            StopCoroutine(_run);

        if (_animate != null)
            StopCoroutine(_animate);

        if (_kill != null)
            StopCoroutine(_kill);

        if (_jump != null)
            StopCoroutine(_jump);
    }

    public void Stop()
    {
        D.Fine("[PlayerController] Stop");
        if (_state == PlayerStates.PLAYER_GROUNDED)
            {
            _animationState = PlayerAnimationStates.PLAYER_ANIM_IDLE;
            _state = PlayerStates.PLAYER_IDLE;
            _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
        }
        else
        {
            _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
        }
    }

    public void Duck()
    {
        D.Trace("[PlayerController] Duck");
        if (_state == PlayerStates.PLAYER_GROUNDED)
        {
            _animationState = PlayerAnimationStates.PLAYER_ANIM_DUCKING;
            _state = PlayerStates.PLAYER_DUCKING;
            _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
        }
    }

    public void Kill(GameObject other, PlayerDeathTypes deathType)
    {
        if (!_running)
            return;

        D.Trace("[PlayerController] Kill");
        D.Detail("- player was killed by {0}", other.name);
        OnStop();
        _kill = StartCoroutine(kill(other, deathType));
    }

    private IEnumerator run()
    {

        D.Trace("[PlayerController] run");
        yield return new WaitForSeconds(1.0f);
        while (_running)
        {

            if (_state == PlayerStates.PLAYER_IDLE)
            {
                GetComponent<CapsuleCollider>().height = 15.0f;
                GetComponent<CapsuleCollider>().center = new Vector3(0, 3.0f, 0);
                GameManager.Instance.CameraController.Offset = new Vector2(0, 25);
            }

            if (_movementState == PlayerMovementStates.PLAYER_MOVE_LEFT)
            {
                transform.position = transform.position + (Vector3.left * MovementSpeed * Time.deltaTime);
            }

            if (_movementState == PlayerMovementStates.PLAYER_MOVE_RIGHT)
            {
                transform.position = transform.position + (Vector3.right * MovementSpeed * Time.deltaTime);
            }

            if (_movementState == PlayerMovementStates.PLAYER_MOVE_IDLE)
            {
                transform.position = transform.position + (Vector3.zero);
            }

            if (_state == PlayerStates.PLAYER_JUMPING)
            {
            }

            if (_state == PlayerStates.PLAYER_DUCKING)
            {
                GetComponent<CapsuleCollider>().height = 10.0f;
                GetComponent<CapsuleCollider>().center = new Vector3(0, -3.0f, 0);
                GameManager.Instance.CameraController.Offset = new Vector2(0, -25);
            }

            if (_state == PlayerStates.PLAYER_FALLING)
            {
                transform.position = transform.position + (Vector3.down * Gravity * Time.deltaTime);
            }

            //  check player position
            if (_movementState == PlayerMovementStates.PLAYER_MOVE_RIGHT)
                checkRight();

            if (_movementState == PlayerMovementStates.PLAYER_MOVE_LEFT)
                checkLeft();

            if (_state != PlayerStates.PLAYER_JUMPING)
            {
                checkBottom();
            }

            D.Watch("Player State", _state);
            D.Watch("Animation State", _animationState);
            D.Watch("Movement State", _movementState);

            yield return null;

        }

        yield return null;
    }

    private IEnumerator animate()
    {
        D.Trace("[PlayerController] animate");

        while (_running)
        {
            if (_animationState == PlayerAnimationStates.PLAYER_ANIM_IDLE)
            {
                _sprite.SetSprite("idle");
            }

            if (_animationState == PlayerAnimationStates.PLAYER_ANIM_JUMPING)
            {
                _animateModule.FramesPerSecond = 10;
                _sprite.SetSprite("jump");
            }

            if (_animationState == PlayerAnimationStates.PLAYER_ANIM_MOVING)
            {
                _animateModule.FramesPerSecond = 20;
                _sprite.SetSprite("move");
            }

            if (_animationState == PlayerAnimationStates.PLAYER_ANIM_DUCKING)
            {
                _sprite.SetSprite("duck");
            }

            yield return null; 
        }

        yield return null;
    }

    private IEnumerator jump()
    {
        D.Trace("[PlayerController] jump");

        float y = transform.position.y;
        float height = y + JumpHeight;
        while(transform.position.y < height && _state == PlayerStates.PLAYER_JUMPING)
        {
            Vector3 pos = transform.position + (Vector3.up * JumpSpeed * Time.deltaTime);
            transform.position = pos;
            yield return new WaitForEndOfFrame();
        }
        _state = _lastState;
        //_state = PlayerStates.PLAYER_FALLING;
        _animationState = _lastAnimationState;
        _movementState = _lastMovementState;
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator kill(GameObject other, PlayerDeathTypes deathType)
    {
        D.Trace("[PlayerController] kill");
        Transform parent = transform.parent;
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return StartCoroutine(deathAnimate(other, deathType));
        transform.parent = parent;
        GameManager.Instance.GameController.Data.Lives -= 1;
        GameManager.Instance.GameController.RestartRoom();
        yield return null;
    }

    private IEnumerator deathAnimate(GameObject other, PlayerDeathTypes deathType)
    {
        D.Trace("[PlayerController] deathAnimate");
        switch (deathType)
        {
            case PlayerDeathTypes.PLAYER_DEATH_BEHEADING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            case PlayerDeathTypes.PLAYER_DEATH_CRUSHING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            case PlayerDeathTypes.PLAYER_DEATH_CUTTING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            case PlayerDeathTypes.PLAYER_DEATH_EXPLODING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            case PlayerDeathTypes.PLAYER_DEATH_IMPALING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            case PlayerDeathTypes.PLAYER_DEATH_MELTING:
                yield return StartCoroutine(deathByImpaling(other));
                break;

            default:
                yield return StartCoroutine(deathByRandom(other));
                break;
        }
        yield return null;
    }

    private IEnumerator deathByRandom(GameObject other)
    {
        D.Trace("[PlayerController] deathByRandom");
        int r = Random.Range(0, 6);
        yield return StartCoroutine(deathAnimate(other, (PlayerDeathTypes)System.Enum.GetValues(typeof(PlayerDeathTypes)).GetValue(r)));
    }

    private IEnumerator deathByImpaling(GameObject other)
    {
        D.Trace("[PlayerController] deathByImpaling");
        D.Detail("- other {0}", other.name);
        transform.parent = other.transform;
        GameObject g = GameManager.Instance.PrefabController.CreatePrefab("BloodRed", transform.position);
        g.transform.parent = transform;
        yield return new WaitForSeconds(2.0f);
        Destroy(g.gameObject);
    }

    private void checkBottom()
    {
        Vector2 rayOrigin = transform.position;
        Debug.DrawRay(rayOrigin, Vector3.down * RaycastLengthBottom, Color.red);
        RaycastHit hit; 
        bool rayhit = Physics.Raycast(rayOrigin, Vector3.down, out hit, RaycastLengthBottom, PlatformLayer);

        //  hit the ground
        if (rayhit)
        {
            if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Platform")
            {
                D.Detail("- bottom hit {0}", LayerMask.LayerToName(hit.collider.gameObject.layer));
                if (_state == PlayerStates.PLAYER_FALLING)
                {
                    GameObject g = GameManager.Instance.PrefabController.CreatePrefab("Dust", transform.position + (Vector3.down * RaycastLengthBottom));
                    Destroy(g, 1.0f);
                }
                _state = PlayerStates.PLAYER_GROUNDED;
                Vector2 pos = transform.position;
                pos.y = hit.point.y + RaycastLengthBottom - PlatformOffset;
                transform.position = pos;
            }

            if (hit.collider.tag == "Lift")
            {
                D.Detail("- bottom hit {0}", LayerMask.LayerToName(hit.collider.gameObject.layer));
                _state = PlayerStates.PLAYER_GROUNDED;
                Vector2 pos = transform.position;
                pos.y = hit.point.y + RaycastLengthBottom - LiftOffset;
                transform.position = pos;
            }
        }
        else
        //  falling
        {
            _state = PlayerStates.PLAYER_FALLING;
        }
    }

    private void checkRight()
    {
        if (_state == PlayerStates.PLAYER_IDLE)
            return;

        Vector2 rayOrigin = transform.position;
        Debug.DrawRay(rayOrigin, Vector3.right * RaycastLengthSides, Color.red);
        RaycastHit hit;
        bool rayhit = Physics.Raycast(rayOrigin, Vector3.right, out hit, RaycastLengthSides, PlatformLayer);

        //  hit the ground
        if (rayhit)
        {
            D.Detail("- right hit {0}", LayerMask.LayerToName(hit.collider.gameObject.layer));
            if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Platform")
            {
                if (_state != PlayerStates.PLAYER_JUMPING)
                {
                    _state = PlayerStates.PLAYER_IDLE;
                    _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
                    _animationState = PlayerAnimationStates.PLAYER_ANIM_IDLE;
                }
                Vector2 pos = transform.position;
                pos.x = hit.point.x - RaycastLengthSides;
                transform.position = pos;
            }
        }
    }

    private void checkLeft()
    {
        if (_state == PlayerStates.PLAYER_IDLE)
            return;

        Vector2 rayOrigin = transform.position;
        Debug.DrawRay(rayOrigin, Vector3.left * RaycastLengthSides, Color.red);
        RaycastHit hit;
        bool rayhit = Physics.Raycast(rayOrigin, Vector3.left, out hit, RaycastLengthSides, PlatformLayer);

        //  hit the ground
        if (rayhit)
        {
            D.Detail("- left hit {0}", LayerMask.LayerToName(hit.collider.gameObject.layer));
            if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Platform")
            {
                if (_state != PlayerStates.PLAYER_JUMPING)
                {
                    _state = PlayerStates.PLAYER_IDLE;
                    _movementState = PlayerMovementStates.PLAYER_MOVE_IDLE;
                    _animationState = PlayerAnimationStates.PLAYER_ANIM_IDLE;
                }
                Vector2 pos = transform.position;
                pos.x = hit.point.x + RaycastLengthSides;
                transform.position = pos;
            }
        }
    }

    void Start()
    {
        D.Trace("[PlayerController] Start");
        Stop();
    }

    void OnEnable()
    {
        D.Trace("[PlayerController] OnEnable");

        GameManager.Instance.PlayerController = this;
        _sprite = GetComponent<SpriteBoss>();
        _animateModule = GetComponent<SpriteBossModuleAnimate>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (!_running)
            return;

        D.Trace("[PlayerController] OnTriggerEnter");
        D.Detail("- hit {0}", other.name);
    }
}
