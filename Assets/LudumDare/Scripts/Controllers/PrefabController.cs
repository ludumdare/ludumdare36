﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabController : MonoBehaviour
{
    public GameObject[] Prefabs;

    private Dictionary<string, GameObject> _prefabs;

    public GameObject CreatePrefab(string name)
    {
        return CreatePrefab(name, transform.position);
    }

    public GameObject CreatePrefab(string name, Vector3 position)
    {
        D.Trace("[PrefabController] CreatePrefab");

        if (_prefabs.ContainsKey(name))
        {
            GameObject g = (GameObject)Instantiate(_prefabs[name], position, Quaternion.identity);
            g.name = string.Format("[Prefab] {0}", name);
            return g;
        }
        else
        {
            D.Warn("- prefab {0} does not exist", name);
            GameObject g = new GameObject();
            g.name = string.Format("[ERROR] !! MissingPrefab({0}) !!", name);
            return g;
        }
    }
        
    void Start()
    {
        D.Trace("[PrefabController] Start");
        _prefabs = new Dictionary<string, GameObject>();

        foreach(GameObject g in Prefabs)
        {
            _prefabs.Add(g.name, g);
        }
    }

    void OnEnable()
    {
        D.Trace("[PrefabController] OnEnable");
        GameManager.Instance.PrefabController = this;
        DontDestroyOnLoad(gameObject);
    }

}
