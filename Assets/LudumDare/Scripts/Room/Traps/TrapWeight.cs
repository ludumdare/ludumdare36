﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TrapWeight : RoomTrap
{
    private Coroutine _run;
    public float StartDelay = 1.0f;
    public float Delay = 1.0f;
    public float Height = 42.0f;

    private Vector3 _bottomPos;

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[TrapWeight] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        StopCoroutine(_run);
        transform.position = _bottomPos;
        GameManager.Instance.PlayerController.Kill(gameObject, PlayerDeathTypes.PLAYER_DEATH_RANDOM);
    }

    public override void Init()
    {
        transform.position = transform.position + (Vector3.down * 5.0f);
        _bottomPos = transform.position + (Vector3.down * Height);
    }

    public override void OnStart()
    {
        _run = StartCoroutine(run());

    }

    public override void OnStop()
    {
        StopCoroutine(_run);
    }

    private IEnumerator run()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(StartDelay);
        while (true)
        {
            yield return new WaitForSeconds(Delay);
            GetComponent<Collider>().enabled = true;
            yield return new WaitForEndOfFrame();
            yield return transform.DOShakePosition(0.5f).WaitForCompletion();
            yield return transform.DOMoveY(transform.position.y - Height, 0.8f).SetEase(Ease.InExpo).WaitForCompletion();
            yield return transform.DOShakePosition(0.2f).WaitForCompletion();
            GetComponent<Collider>().enabled = false;
            yield return new WaitForEndOfFrame();
            yield return transform.DOMoveY(transform.position.y + Height, 0.5f).SetEase(Ease.Linear).WaitForCompletion();

        }
    }

}
