﻿using UnityEngine;
using System.Collections;

public class TrapSpikes : RoomTrap
{
    public override void TriggerEnter(Collider other)
    {
        D.Trace("[TrapSpikes] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        GameManager.Instance.PlayerController.Kill(gameObject, PlayerDeathTypes.PLAYER_DEATH_RANDOM);
    }

}
