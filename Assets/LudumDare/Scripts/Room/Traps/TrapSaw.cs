﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TrapSaw : RoomTrap
{
    private Coroutine _run;
    public float StartDelay = 1.0f;
    public float Distance = 60.0f;
    public float Height = 20.0f;
    public float Speed = 1.0f;

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[TrapSaw] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        GameManager.Instance.PlayerController.Kill(gameObject, PlayerDeathTypes.PLAYER_DEATH_RANDOM);
    }

    public override void Init()
    {
        transform.position = transform.position + (Vector3.down * 15.0f);
    }

    public override void OnStart()
    {
        _run = StartCoroutine(run());

    }

    public override void OnStop()
    {
        StopCoroutine(_run);
    }

    private IEnumerator run()
    {
        yield return new WaitForEndOfFrame();
        Vector3 r = new Vector3(0, 0, 360);
        transform.DORotate(r, 0.25f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Restart).SetEase(Ease.Linear);
        yield return new WaitForSeconds(StartDelay);
        while (true)
        {
            yield return transform.DOMoveX(transform.position.x + Distance, Speed).SetEase(Ease.Linear).WaitForCompletion();
            yield return transform.DOMoveY(transform.position.y + Height, Speed).SetEase(Ease.Linear).WaitForCompletion();
            yield return transform.DOMoveX(transform.position.x - Distance, Speed).SetEase(Ease.Linear).WaitForCompletion();
            yield return transform.DOMoveY(transform.position.y - Height, Speed).SetEase(Ease.Linear).WaitForCompletion();

        }
    }

}
