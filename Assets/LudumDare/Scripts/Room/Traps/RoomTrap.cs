﻿using UnityEngine;
using System.Collections;

public class RoomTrap : MonoBehaviour
{

    public virtual void Init()
    {
        D.Trace("[RoomTrap] Init");
    }

    public virtual void OnStart()
    {
        D.Trace("[RoomTrap] OnStart");
    }

    public virtual void OnStop()
    {
        D.Trace("[RoomTrap] OnStop");
    }

    public virtual void TriggerEnter(Collider other)
    {
        D.Trace("[RoomTrap] TriggerEnter");
        D.Trace("- hit by {0}", other.name);
        GameManager.Instance.PlayerController.Kill(other.gameObject, PlayerDeathTypes.PLAYER_DEATH_RANDOM);
    }

    void OnTriggerEnter(Collider other)
    {
        D.Trace("[RoomTrap] OnTriggerEnter");
        //GetComponent<Collider>().enabled = false;
        TriggerEnter(other);
    }

    void OnEnable()
    {
        D.Trace("[RoomTrap] OnEnable");
        GameManager.Instance.GameController.RegisterRoomTrap(this);
    }

}
