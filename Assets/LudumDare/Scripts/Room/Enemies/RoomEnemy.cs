﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RoomEnemy : MonoBehaviour
{
    public float Speed;

    private GameObject _waypoint;
    private Vector3 _lastPos;
    private Vector3 _startPos;
    private SpriteRenderer _spriteRenderer;
    private Tween _tween;

    public virtual void Init()
    {
        D.Trace("[RoomEnemy] Init");
        transform.position = _startPos;
    }

    public virtual void OnStart()
    {
        D.Trace("[RoomEnemy] OnStart");
        if (_waypoint != null)
        {
            _tween = transform.DOMove(_waypoint.transform.position, Speed)
                .SetSpeedBased(true)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.Linear);
        }
    }

    public virtual void OnStop()
    {
        D.Trace("[RoomEnemy] OnStop");
        _tween.Kill();
    }

    public virtual void TriggerEnter(Collider other)
    {
        D.Trace("[RoomEnemy] TriggerEnter");
        D.Trace("- hit by {0}", other.name);
        GameManager.Instance.PlayerController.Kill(gameObject, PlayerDeathTypes.PLAYER_DEATH_RANDOM);
    }

    void OnTriggerEnter(Collider other)
    {
        D.Trace("[RoomEnemy] OnTriggerEnter");
        //GetComponent<Collider>().enabled = false;
        TriggerEnter(other);
    }

    void Update()
    {
        Vector3 dir = transform.position - _lastPos;
        _lastPos = transform.position;
        if (dir.x > 0)
        {
            _spriteRenderer.flipX = true;
        }
        else
        {
            _spriteRenderer.flipX = false;
        }
    }

    void Awake()
    {
        _startPos = transform.position;
    }

    void OnEnable()
    {
        D.Trace("[RoomEnemy] OnEnable");
        GameManager.Instance.GameController.RegisterRoomEnemy(this);
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _waypoint = (GameObject)transform.Find("Waypoint").gameObject;
        if (_waypoint == null)
        {
            D.Warn("- {0} is missing an attached Waypoint GameObject", gameObject.name);
        }
        else
        {
            _waypoint.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
