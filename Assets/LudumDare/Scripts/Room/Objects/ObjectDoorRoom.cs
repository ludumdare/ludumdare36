﻿using UnityEngine;
using System.Collections;

public class ObjectDoorRoom : RoomObject
{
    public DoorData Door;
    public Sprite DoorOpenSprite;
    public Sprite DoorClosedSprite;

    public bool DoorOpen { get; set; }

    private SpriteRenderer _sprite;

    public void Open()
    {
        D.Trace("[ObjectDoorRoom] Open");
        _sprite.sprite = DoorOpenSprite;
        GetComponent<BoxCollider>().enabled = true;
        DoorOpen = true;
    }

    public void Close()
    {
        D.Trace("[ObjectDoorRoom] Close");
        _sprite.sprite = DoorClosedSprite;
        GetComponent<BoxCollider>().enabled = false;
        DoorOpen = false;
    }

    public override void Init()
    {
        D.Trace("[ObjectDoorRoom] Init");
        if (DoorOpen)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[ObjectDoorRoom] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        D.Detail("- going to room {0}", Door.Room);

        StartCoroutine(openDoor());
    }

    private IEnumerator openDoor()
    {
        D.Trace("[ObjectDoorRoom] openDoor");
        GameManager.Instance.PlayerController.OnStop();
        yield return new WaitForEndOfFrame();
        GameManager.Instance.GameController.StopRoom();
        D.Trace("- opening room {0}", Door.Room);
        GameManager.Instance.GameController.OpenRoom(Door.Room);
        GameManager.Instance.GameController.StartRoom();
    }
    void OnEnable()
    {
        D.Trace("[ObjectDoorRoom] OnEnable");
        _sprite = GetComponent<SpriteRenderer>();
    }
}
