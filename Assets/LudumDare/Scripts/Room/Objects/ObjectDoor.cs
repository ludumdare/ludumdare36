﻿using UnityEngine;
using System.Collections;

public class ObjectDoor : RoomObject
{
    public DoorData Door;
    public Sprite DoorOpenSprite;
    public Sprite DoorClosedSprite;

    public bool DoorOpen { get; set; }

    private SpriteRenderer _sprite;

    public void Open()
    {
        D.Trace("[ObjectDoor] Open");
        _sprite.sprite = DoorOpenSprite;
        GetComponent<BoxCollider>().enabled = true;
        DoorOpen = true;
    }

    public void Close()
    {
        D.Trace("[ObjectDoor] Close");
        _sprite.sprite = DoorClosedSprite;
        GetComponent<BoxCollider>().enabled = false;
        DoorOpen = false;
    }

    public override void Init()
    {
        D.Trace("[ObjectDoor] Init");
        if (DoorOpen)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[ObjectDoor] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        D.Detail("- going to room {0}", Door.Room);

        if (DoorOpen)
        {
            //  setup for next room - really bad code here!

            if (Door.Room == "Room1")
            {
                GameManager.Instance.GameController.Data.Door1State = DoorStates.DOOR_STATE_COMPLETE;
                GameManager.Instance.GameController.Data.Door2State = DoorStates.DOOR_STATE_ACTIVE;
            }

            if (Door.Room == "Room2")
            {
                GameManager.Instance.GameController.Data.Door2State = DoorStates.DOOR_STATE_COMPLETE;
                GameManager.Instance.GameController.Data.Door3State = DoorStates.DOOR_STATE_ACTIVE;
            }

            if (Door.Room == "Room3")
            {
                GameManager.Instance.GameController.Data.Door3State = DoorStates.DOOR_STATE_COMPLETE;
                GameManager.Instance.GameController.Data.Door4State = DoorStates.DOOR_STATE_ACTIVE;
            }

            if (Door.Room == "Room4")
            {
                GameManager.Instance.GameController.Data.Door4State = DoorStates.DOOR_STATE_COMPLETE;
                GameManager.Instance.GameController.Data.DoorFinalState = DoorStates.DOOR_STATE_ACTIVE;
            }

            StartCoroutine(openDoor());
        }
    }

    private IEnumerator openDoor()
    {
        D.Trace("[ObjectDoor] openDoor");
        GameManager.Instance.PlayerController.OnStop();
        yield return new WaitForEndOfFrame();
        GameManager.Instance.GameController.StopRoom();
        D.Trace("- opening room {0}", Door.Room);
        GameManager.Instance.GameController.OpenRoom(Door.Room);
        GameManager.Instance.GameController.StartRoom();
    }
    void OnEnable()
    {
        D.Trace("[ObjectDoor] OnEnable");
        _sprite = GetComponent<SpriteRenderer>();
    }
}
