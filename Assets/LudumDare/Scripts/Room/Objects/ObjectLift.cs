﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ObjectLift : RoomObject
{
    private bool _lifting;
    private bool _entered;

    /*
    public bool Entered
    {
        get { return _entered; }
        set { _entered = value; }
    }
    */

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[ObjectLift] TriggerEnter");

        if (_lifting)
            return;

        /*
        if (_entered)
            return;

        _entered = true;
        */

        StartCoroutine(moveUp());
    }

    public override void TriggerExit(Collider other)
    {
        D.Trace("[ObjectLift] TriggerExit");
        //_entered = false;
    }

    public override void Init()
    {
        D.Trace("[ObjectLift] Init");
        _lifting = false;
        //_entered = false;
    }

    private IEnumerator moveUp()
    {
        D.Trace("[ObjectLift] moveUp");

        _lifting = true;
        GameManager.Instance.PlayerController.Stop();
        yield return new WaitForFixedUpdate();
        GameManager.Instance.PlayerController.transform.position = transform.position + (Vector3.up * 10.0f);
        yield return new WaitForFixedUpdate();
        yield return transform.DOMoveY(transform.position.y + 60.0f, 1.0f).WaitForCompletion();
        yield return new WaitForSeconds(1.0f);
        yield return transform.DOMoveY(transform.position.y - 60.0f, 0.05f).WaitForCompletion();
        yield return new WaitForSeconds(1.5f);
        _lifting = false;
        yield return null;
    }
}
