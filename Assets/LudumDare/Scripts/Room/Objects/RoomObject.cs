﻿using UnityEngine;
using System.Collections;

public class RoomObject : MonoBehaviour
{

    public virtual void Init()
    {
        D.Trace("[RoomObject] Init");
    }

    public virtual void OnStart()
    {
        D.Trace("[RoomObject] OnStart");
    }

    public virtual void OnStop()
    {
        D.Trace("[RoomObject] OnStop");
    }

    public virtual void TriggerEnter(Collider other)
    {
        D.Trace("[RoomObject] TriggerEnter");
        D.Trace("- hit by {0}", other.name);
    }

    public virtual void TriggerExit(Collider other)
    {
        D.Trace("[RoomObject] TriggerExit");
        D.Trace("- {0} left {1}", other.name, gameObject.name);
    }

    void OnTriggerEnter(Collider other)
    {
        D.Trace("[RoomObject] OnTriggerEnter");
        TriggerEnter(other);
    }

    void OnTriggerExit(Collider other)
    {
        D.Trace("[RoomObject] OnTriggerExit");
        TriggerExit(other);
    }

    void OnEnable ()
    {
        GameManager.Instance.GameController.RegisterRoomObject(this);
	}
}
