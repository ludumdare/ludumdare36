﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ObjectNewLift : RoomObject
{

    public float Speed;

    private GameObject _waypoint;
    private Vector3 _lastPos;
    private Vector3 _startPos;
    private SpriteRenderer _spriteRenderer;
    private Tween _tween;

    public override void Init()
    {
        D.Trace("[ObjectNewLift] Init");
        transform.position = _startPos;
    }

    public override void OnStart()
    {
        D.Trace("[ObjectNewLift] OnStart");
        if (_waypoint != null)
        {
            _tween = transform.DOMove(_waypoint.transform.position, Speed)
                .SetSpeedBased(true)
                .SetLoops(-1, LoopType.Yoyo)
                .SetEase(Ease.Linear);
        }
    }

    public override void OnStop()
    {
        D.Trace("[ObjectNewLift] OnStop");
        _tween.Kill(); 
    }


    void OnEnable()
    {
        D.Trace("[RoomEnemy] OnEnable");
        GameManager.Instance.GameController.RegisterRoomObject(this);
        _startPos = transform.position;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _waypoint = (GameObject)transform.Find("Waypoint").gameObject;
        if (_waypoint == null)
        {
            D.Warn("- {0} is missing an attached Waypoint GameObject", gameObject.name);
        }
        else
        {
            _waypoint.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
