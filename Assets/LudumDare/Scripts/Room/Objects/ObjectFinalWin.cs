﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;

public class ObjectFinalWin : RoomObject
{
    public GameObject Spaceship;
    public GameObject Booster1;
    public GameObject Booster2;
    public GameObject Latch;
    public override void TriggerEnter(Collider other)
    {
        StartCoroutine(run());
    }

    private IEnumerator run()
    {
        GameObject s1 = GameManager.Instance.SoundController.PlaySound("Spaceship", true);
        yield return null;
        //  hide player
        GameManager.Instance.PlayerController.Stop();
        GameManager.Instance.PlayerController.OnStop();
        Transform parent = GameManager.Instance.PlayerController.transform.parent;
        GameManager.Instance.PlayerController.transform.parent = Spaceship.transform;
        GameManager.Instance.PlayerController.GetComponent<SpriteRenderer>().enabled = false;
        //  start lifting
        yield return Spaceship.transform.DOMoveY(Spaceship.transform.position.y + 25.0f, 3.0f).WaitForCompletion();
        yield return new WaitForSeconds(1.0f);
        //  close the latch
        GameObject sound = GameManager.Instance.SoundController.PlaySound("Latch", true);
        yield return Latch.transform.DOLocalMoveY(0, 3.0f).WaitForCompletion();
        Latch.GetComponent<SpriteRenderer>().enabled = false;
        Destroy(sound);
        yield return new WaitForSeconds(1.0f);
        //  show rocket boosters
        Booster1.GetComponent<SpriteRenderer>().enabled = true;
        Booster2.GetComponent<SpriteRenderer>().enabled = true;
        Booster1.transform.DOLocalMoveY(-10.0f, 3.0f);
        Booster2.transform.DOLocalMoveY(-10.0f, 3.0f);
        yield return new WaitForSeconds(4.0f);
        //  show particles
        GameObject b1 = GameManager.Instance.PrefabController.CreatePrefab("Booster", Booster1.transform.position + (Vector3.down * 10.0f));
        GameObject b2 = GameManager.Instance.PrefabController.CreatePrefab("Booster", Booster2.transform.position + (Vector3.down * 10.0f));
        b1.transform.parent = Booster1.transform;
        b2.transform.parent = Booster2.transform;
        sound = GameManager.Instance.SoundController.PlaySound("Booster", true);
        yield return Spaceship.transform.DOShakePosition(8.0f).WaitForCompletion();
        //  take off very fast
        Spaceship.transform.DOMoveY(500.0f, 10.0f);
        yield return new WaitForSeconds(2.0f);
        Destroy(b1);
        Destroy(b2);
        GameManager.Instance.PlayerController.transform.parent = parent;
        //  load win scene
        yield return new WaitForSeconds(6.0f);
        Destroy(sound);
        Destroy(s1);
        SceneManager.LoadScene("Win");
    }

    public override void Init()
    {
        Booster1.GetComponent<SpriteRenderer>().enabled = false;
        Booster2.GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
