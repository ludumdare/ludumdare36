﻿using UnityEngine;
using System.Collections;

public class ObjectDoorPortal : RoomObject
{
    public DoorData Door;
    public override void TriggerEnter(Collider other)
    {
        D.Trace("[ObjectDoorPortal] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        D.Detail("- going to room {0} door {1}", Door.Room, Door.Door);
        StartCoroutine(openDoor());
    }

    private IEnumerator openDoor()
    {
        GameManager.Instance.PlayerController.OnStop();
        yield return new WaitForEndOfFrame();
        GameManager.Instance.GameController.StopRoom();
        GameManager.Instance.GameController.OpenRoom(Door.Room);
        GameManager.Instance.GameController.StartRoom();
    }
}
