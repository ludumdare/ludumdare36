﻿using UnityEngine;
using System.Collections;

public class ObjectDoorFinal : RoomObject
{

    public DoorData Door;
    public Sprite DoorOpenSpriteL;
    public Sprite DoorOpenSpriteR;
    public Sprite DoorClosedSpriteL;
    public Sprite DoorClosedSpriteR;

    public bool DoorOpen { get; set; }

    private SpriteRenderer _spriteL;
    private SpriteRenderer _spriteR;

    public void Open()
    {
        D.Trace("[ObjectDoorFinal] Open");
        _spriteL.sprite = DoorOpenSpriteL;
        _spriteR.sprite = DoorOpenSpriteR;
        GetComponent<BoxCollider>().enabled = true;
    }

    public void Close()
    {
        D.Trace("[ObjectDoorFinal] Close");
        _spriteL.sprite = DoorClosedSpriteL;
        _spriteR.sprite = DoorClosedSpriteR;
        GetComponent<BoxCollider>().enabled = false;
    }

    public override void Init()
    {
        D.Trace("[ObjectDoorFinal] Init");
        if (DoorOpen)
        {
            Open();
        }
        {
            Close();
        }
    }

    public override void TriggerEnter(Collider other)
    {
        D.Trace("[ObjectDoorFinal] OnTrigger");
        D.Detail("- hit by {0}", other.name);
        D.Detail("- going to room {0}", Door.Room);
        StartCoroutine(openDoor());
    }

    private IEnumerator openDoor()
    {
        GameManager.Instance.PlayerController.OnStop();
        yield return new WaitForEndOfFrame();
        GameManager.Instance.GameController.StopRoom();
        GameManager.Instance.GameController.OpenRoom(Door.Room);
        GameManager.Instance.GameController.StartRoom();
    }
    void OnEnable()
    {
        D.Trace("[ObjectDoorFinal] OnEnable");
        _spriteL = (SpriteRenderer)transform.Find("DoorL").GetComponent<SpriteRenderer>();
        _spriteR = (SpriteRenderer)transform.Find("DoorR").GetComponent<SpriteRenderer>();
    }
}
