﻿using UnityEngine;
using System.Collections;

public class ObjectPlayerStart : RoomObject
{
    private PlayerController _player;

    public override void Init()
    {
        D.Trace("[PlayerStart] Init");
        _player = GameManager.Instance.PlayerController;
        _player.transform.position = Vector2.one * -10000.0f;
    }

    public override void OnStart()
    {
        D.Trace("[PlayerStart] OnStart");
        _player.transform.position = transform.position;
   }

    public override void OnStop()
    {
        D.Trace("[PlayerStart] OnStop");
    }

    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
