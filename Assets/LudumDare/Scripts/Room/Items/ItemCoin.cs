﻿using UnityEngine;
using System.Collections;

public class ItemCoin : RoomItem
{
    public override void TriggerEnter(Collider other)
    {
        GameManager.Instance.SoundController.PlaySound("Coin");
    }
}
