﻿using UnityEngine;
using System.Collections;

public class RoomItem : MonoBehaviour
{

    public int Score;

    public virtual void Init()
    {
        D.Trace("[RoomItem] Init");
    }

    public virtual void OnStart()
    {
        D.Trace("[RoomItem] OnStart");
    }

    public virtual void OnStop()
    {
        D.Trace("[RoomItem] OnStop");
    }

    public virtual void TriggerEnter(Collider other)
    {
        D.Trace("[RoomItem] TriggerEnter");
        D.Trace("- hit by {0}", other.name);
        GameManager.Instance.SoundController.PlaySound("Pickup");
    }

    void OnTriggerEnter(Collider other)
    {
        D.Trace("[RoomItem] OnTriggerEnter");
        GameManager.Instance.GameController.Data.Score += Score;
        TriggerEnter(other);
        gameObject.SetActive(false);
    }

    void Start()
    {

    }

    void OnEnable()
    {
        GameManager.Instance.GameController.RegisterRoomItem(this);
    }
}
