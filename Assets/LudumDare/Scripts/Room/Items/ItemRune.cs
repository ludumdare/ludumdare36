﻿using UnityEngine;
using System.Collections;

public class ItemRune : RoomItem
{

    public ObjectDoorRoom Door;

    public override void TriggerEnter(Collider other)
    {
        GameManager.Instance.SoundController.PlaySound("Rune");
        Door.Open();
    }
}
